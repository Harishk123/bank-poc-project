/**----------------------------------------------------------------------------------------------
 * @Author : Harish Khodake(PwC)
 * @Company : PWC India
 * @Description: : Apex class to get BankAccount Openingform Details Form Metadata  and Save  them on Bank Cutomer Details
 * @Test class: : FetchDataFromMetadataTest
 * @Test class: : Completed 
 * @Last modified on : 06-05-2023
 * @last modified by : Harish Khodake(PwC)
 -----------------------------------------------------------------------------------------------*/


public with sharing class FetchDataFromMetadata{
    @AuraEnabled
    public static List<Bank_Cutomer_Details__c> getVerificationData(string mobileNo){
        try {
            List<Bank_Cutomer_Details__c> mobileData = [select id,Mobile_No__c,Last_Name__c,First_Name__c from Bank_Cutomer_Details__c where  Mobile_No__c=:mobileNo WITH SECURITY_ENFORCED Limit 1 ];
            if(mobileData!=null){
                
                return mobileData;
            }else{
                return null;
            }
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            return null;
        }
        
        
    }
    @AuraEnabled
    public static string getData(){
        try {
            List<Bank_Form__mdt> bankData = [SELECT Id, Questions__c, Question_Type__c,Response_values__c
                                             ,IsActive__c,Serial_No__c,IsAddress__c,IsPersonalInfo__c,
                                             IsEducationDetails__c,ObjectApiName__c,FieldApiName__c,IsAccountInfo__c,IsRequired__c
                                             FROM Bank_Form__mdt  WHERE 
                                             IsActive__c=true  WITH SECURITY_ENFORCED
                                             ORDER BY Serial_No__c  ];
        
            string  josondata = JSoN.serialize(bankData);
           
            return josondata;
            
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            return null;
        }
        
    }
    
    @AuraEnabled
    public static Bank_Cutomer_Details__c saveData(map<String , String> allResultMap){
        try {
            system.debug('allResultMap>>'+allResultMap);
            // List<Bank_Cutomer_Details__c> insertBank = new List<Bank_Cutomer_Details__c>();
            Bank_Cutomer_Details__c bank = new Bank_Cutomer_Details__c();
            bank.Prefix__c =  String.valueOf(allResultMap.get('Prefix'));
            bank.First_Name__c = String.valueOf(allResultMap.get('First Name'));
            bank.Last_Name__c =  String.valueOf(allResultMap.get('Last Name'));    	
            bank.Date_of_Birth__c =  allResultMap.get('Date of Birth') != Null ? Date.valueOf(allResultMap.get('Date of Birth')) : Null;
            bank.Months__c = string.valueOf(allResultMap.get('Months'));
            bank.Day__c = string.valueOf(allResultMap.get('Day'));
            bank.Year__c = string.valueOf(allResultMap.get('Year'));
            bank.Mothers_Name__c = string.valueOf(allResultMap.get('Mothers Name'));
            bank.Mobile_No__c = String.valueOf(allResultMap.get('Mobile No'));
            bank.Email__c = String.valueOf(allResultMap.get('Email'));
            bank.Line_1__c = String.valueOf(allResultMap.get('Line 1'));
            bank.Line_2__c = String.valueOf(allResultMap.get('Line 2'));
            bank.Street__c = String.valueOf(allResultMap.get('Street'));
            bank.Country__c = String.valueOf(allResultMap.get('Country'));
            bank.UnderGraduate__c = allResultMap.get('UnderGraduate') == 'true' ? true : false;
            bank.High_School__c = allResultMap.get('High School')== 'true' ? true : false;
            bank.Masters__c = allResultMap.get('Masters')== 'true' ? true : false;
            bank.Primary__c = allResultMap.get('Primary')== 'true' ? true : false;
            bank.Account_Type__c = String.valueOf(allResultMap.get('Account Type'));
            bank.Account_Category__c = String.valueOf(allResultMap.get('Account Category'));
            
            insert bank;
            return bank;
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            return null;
        }
       
      
        }    
}