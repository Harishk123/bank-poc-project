/**----------------------------------------------------------------------------------------------
 * @Author : Harish Khodake(PwC)
 * @Company : PWC India
 * @Description: : Apex class to get the Hospitals details which are located to perticular userinput location
 * @Last modified on : 27-06-2023
 * @last modified by : Harish Khodake(PwC)
 -----------------------------------------------------------------------------------------------*/


public class HospitalInformationController {
    
  @AuraEnabled
    public static List<Hospital__c> gethospitalDetail(integer distance,string hospitalType,double latitudevalue,double longitudevalue){
        try{
        system.debug('distance '+distance+'hospitalType '+hospitalType+'latitudevalue '+latitudevalue+ 'longitudevalue'+longitudevalue);
        List<Hospital__c> hospitl = new List<Hospital__c>();
        hospitl =[SELECT Id, Name, Hospital_Location__c
                  FROM Hospital__c 
                 WHERE DISTANCE(Hospital_Location__c, GEOLOCATION(:latitudevalue,:longitudevalue), 'km')> :distance AND 
                  Hospital_Type__c=:hospitalType];
        
        system.debug('hospitl>>'+hospitl);
        if(hospitl.size()>0){
             return hospitl;
        }else{
          return null;  
        }
            } catch (Exception e) {
        throw new AuraHandledException(e.getMessage());
       
    }
    }

       @AuraEnabled
    public static List<Hospital__c> getHospitalType(){
        try{
        List<Hospital__c> hosptlType = new List<Hospital__c>();
        
        hosptlType=[SELECT Id, Hospital_Type__c FROM Hospital__c];
        if(hosptlType.size()>0){
            system.debug('hosptlType'+hosptlType);
            return hosptlType;
        }else{
         return null;   
        }
              } catch (Exception e) {
        throw new AuraHandledException(e.getMessage()); 
    }

}
}
