@RestResource(urlMapping='/data')
global class DataRestController {
    
    @HttpGet
    global static void getData() {
        RestResponse response = RestContext.response;
        RestRequest request = RestContext.request;
        
        try {
        
            Decimal latitudeValue = Decimal.valueOf(request.params.get('latitudevalue'));
            Decimal longitudeValue = Decimal.valueOf(request.params.get('longitudevalue'));
            Decimal distance = Decimal.valueOf(request.params.get('distance'));
            String hospitalType = request.params.get('hospitalType');
          //  String hospitalwebsite = request.params.get('hospitalwebsite');
            List<Map<String, Object>> results = new List<Map<String, Object>>();
            
      
            String query = 'SELECT Id, Name, Hospital_Location__c FROM Hospital__c ' +
                           'WHERE DISTANCE(Hospital_Location__c, GEOLOCATION(:latitudeValue, :longitudeValue), \'km\') > :distance ' +
                           'AND Hospital_Type__c = :hospitalType';
            List<Hospital__c> hospitals = Database.query(query);
            system.debug('hospitals'+hospitals);
            for (Hospital__c hospital : hospitals) {
                results.add(new Map<String, Object> {
                  
                    'Name' => hospital.Name,
                    'Hospital_Location__c' => hospital.Hospital_Location__c
                });
            }
            
            response.statusCode = 200;
            response.responseBody = Blob.valueOf(JSON.serialize(results));
        } catch (Exception e) {
            response.statusCode = 500;
            response.responseBody = Blob.valueOf(e.getMessage());
        }
    }
}