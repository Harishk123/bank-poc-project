import { LightningElement,track} from 'lwc';
import verifyMobNo from '@salesforce/apex/FetchDataFromMetadata.getVerificationData';
import getData from '@salesforce/apex/FetchDataFromMetadata.getData';
import saveData from '@salesforce/apex/FetchDataFromMetadata.saveData';	
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
export default class BankAccountOpeningForm extends LightningElement {

//These are the List of variables used. 
@track accountNotExixt;
@track mobVerification=true;
@track accountExist;
@track verifyMobile;
@track applicantFirstName;
@track applicantLastName;
@track applicantID;
@track buttonAccountOpen=true;
@track showLabelonhandle;
@track updatedFormData=[];
@track res;
@track allData={};
//@track isrequired=false;

//  connectedCallback(){
//     console.log('this.requiredValue ',this.requiredValue); 
//  }
    handleVerificationCheck(event){
        //This handleVerificationCheck is used to get values of mobile no.
    this.verifyMobile = event.target.value;
    
  
    }
    handleBack(){
       //This handleBack is used to Move on privious screen which is verification screen.
        this.mobVerification=true;
        this.accountExist=false;
    }

    handleCheck(){
        //From This handleCheck We are verifiying Account is Already Created or Not in Database.
        verifyMobNo({mobileNo:this.verifyMobile}).then(result => {
            if(result.length>0){
                const event = new ShowToastEvent({
                    title: 'error',
                    message: 'Bank Account Already Exist!!',
                    variant: 'error',
                    mode: 'dismissable'
                });
                this.dispatchEvent(event);

                this.applicantFirstName = result[0].First_Name__c;
                this.applicantLastName = result[0].Last_Name__c;
                this.applicantID = result[0].Id;
                this.accountExist = true;
               this.mobVerification = false;
            }else{
                this.accountNotExixt = true;
                this.mobVerification = false;
            }

        });
     
    }
    handleLoad() {
        //From This handleLoad we are getting all Metadata from Database
        this.buttonAccountOpen =false;
        this.showLabelonhandle=true;
        getData()
        .then(result => {
        this.res = JSON.parse(result);
        
        this.res.forEach(data => {
     
       // console.log('isdataRequired>>>>>>#', (data.IsRequired__c==="true"));
        let comboval =[];
            if(data.Response_values__c){
                JSON.parse(data.Response_values__c).forEach(data1 => {
                    comboval.push({label:data1.label,value:data1.value});
                });
            }
            //This object is used to render the metadata values on HTML.  
           
        const updatedData = {
            Id: data.Id,
            Questions__c: data.Questions__c,
            Question_Type__c: data.Question_Type__c,
            IsActive__c: data.IsActive__c,
            Serial_No__c: data.Serial_No__c,
            Response_values__c:comboval,
            IsAddress__c: data.IsAddress__c,
            IsPersonalInfo__c: data.IsPersonalInfo__c,
            IsEducationDetails__c:data.IsEducationDetails__c,
            IsAccount__c:data.IsAccountInfo__c,
            isrequired:(data.IsRequired__c==="true")
            
        };
          
        //This is used to check the data or to seperate the data on type.
        if (data.Question_Type__c === 'text' || data.Question_Type__c === 'datetime' || data.Question_Type__c === 'date'|| data.Question_Type__c === 'email'|| data.Question_Type__c === 'email'|| data.Question_Type__c === 'checkbox') {
            updatedData.isText = true;
        } else if (data.Question_Type__c === 'Combobox') {
            updatedData.isCheckbox = true;
        
            updatedData.options=comboval;
        }
        
        this.updatedFormData.push(updatedData);
        
        });
        })
        .catch(error => {
            this.error = error;
        }); 
    }

    handleInputChangeDynamically(event){
        //This handleInputChangeDynamically is used to store the changed input values
       
        let fieldName = event.target.label;
        let fieldValue = event.target.value;
        if(event.target.type=="checkbox"){
            fieldValue = event.target.checked;
        }
        this.allData = {...this.allData, [fieldName]: fieldValue};
    }

    handleSave(){
              
let fieldErrorMsg1="Please Enter the";
this.template.querySelectorAll(["lightning-input","lightning-combobox"]).forEach(item => {
    let fieldValue=item.value;
   let arrayofObject=[];
   arrayofObject.push(fieldValue);
   console.log('arrayofObject',arrayofObject.length);
    let fieldLabel=item.label; 
    
     if(item.required==true){
      

       
     if(!fieldValue){
        item.setCustomValidity(fieldErrorMsg1+' '+fieldLabel);
        
     }
     else if(fieldValue){
        //From This Method we are Creating Record
        saveData({
            allResultMap : this.allData
           
        })
        .then(result => {
            this.fromApex = result;
            console.log('result>>>>>',result);
            
            const event = new ShowToastEvent({
                title: 'success',
                message: 'Bank Account Form Submitted Successfully!!',
                variant: 'success',
                mode: 'dismissable'
            });
            this.dispatchEvent(event);
        
            console.log('line73>>', JSON.stringify(this.fromApex));

        }).catch(error => {
          
        });
     }
 
    else{
        item.setCustomValidity("");  
    }
    item.reportValidity();

}
});

    }
  
}