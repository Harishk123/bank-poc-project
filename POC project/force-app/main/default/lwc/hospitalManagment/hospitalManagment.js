import { LightningElement, track, wire } from 'lwc';
import gethospitalDetail from '@salesforce/apex/HospitalInformationController.gethospitalDetail';
import getHospitalType from '@salesforce/apex/HospitalInformationController.getHospitalType';


export default class HospitalManagment extends LightningElement {
    @track optionArray = [];
    @track updatedData;
    @track uniqueData;
    @track value;
    @track distance;
    @track latVal;
    @track longVal;
    @track markerVar;
    @track myarrayOfmap = [];
    @track latitudeval;
    @track longitudeval;
    @track hospitalExist = true;
    @track hospitalname;
    @track noHospital;
    @track hospital;
    @track hospitalType;



    connectedCallback() {
        try {
            getHospitalType().then(result => {

                let resOfHospitalType = result;

                if (resOfHospitalType.length > 0) {

                    for (let i = 0; i < resOfHospitalType.length; i++) {

                        let options = { label: resOfHospitalType[i].Hospital_Type__c, value: resOfHospitalType[i].Hospital_Type__c }

                        this.optionArray.push(options);
                        this.uniqueData = Array.from(new Set(this.optionArray.map(obj => obj.value)))
                            .map(value => this.optionArray.find(obj => obj.value === value));
                    }
                    this.updatedData = this.uniqueData.map(obj => {
                        const label = obj.label;
                        const value = obj.value;
                        return { label, value };
                    });

                }

            })

        } catch (e) {
            console.error(e);
        }

    }

    handleChange(event) {
        this.value = event.detail.value;

    }
    inputDistance(event) {
        this.distance = event.detail.value;

    }

    handleClick() {
        try {

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(position => {

                    this.latVal = position.coords.latitude;
                    this.longVal = position.coords.longitude;

                    this.markerVar = [{
                        location: {
                            Latitude: this.latVal,
                            Longitude: this.longVal

                        },
                        title: 'Your Current Location '

                    }];
                    let userlocation = {
                        location: {
                            Latitude: this.latVal.toString(),
                            Longitude: this.longVal.toString(),

                        },
                        title: 'Your Current Location '
                    }
                    this.myarrayOfmap.push(userlocation);
                });

            }
        } catch (e) {
            console.error(e);
        }

    }

    handleSubmit() {
        try {
           
            gethospitalDetail({ distance: this.distance, hospitalType: this.value, latitudevalue: this.latVal, longitudevalue: this.longVal }).then(result => {

                if (result != null) {

                    for (let i = 0; i < result.length; i++) {
                        this.hospitalname = result[i].Name;
                        this.latitudeval = result[i].Hospital_Location__c.latitude;
                        this.longitudeval = result[i].Hospital_Location__c.longitude;

                        let mapobject = {
                            location: {
                                Latitude: this.latitudeval.toString(),
                                Longitude: this.longitudeval.toString(),

                            },
                            title: this.hospitalname

                        }

                        this.myarrayOfmap.push(mapobject);

                    }
                    if (this.myarrayOfmap.length > 0) {
                        this.hospitalExist = false;
                        this.hospital = true;
                        this.noHospital = false;
                    }


                } else {
                    this.noHospital = true;
                    this.hospitalExist = false;
                    this.hospital = false;
                }

            });
        
        } catch (e) {
            console.error(e);
        }

    }

    handleLatitudeChange(event) {
        try {
            let userInputlatValues = event.detail.value;
            if (this.isValidLatitude(userInputlatValues)) {
                this.latVal = userInputlatValues;

            }  else {

                console.error('Invalid latitude value');
            }
        } catch (e) {
            console.error(e);
        }
    }

    handleLongitudeChange(event) {
        try {
            let userInputlongValues = event.detail.value;
            if (this.isValidLatitude(userInputlongValues)) {
                this.longVal = userInputlongValues;

            }
             else {

                console.error('Invalid longitude value');
            }
        } catch (e) {
            console.error(e);
        }
    }

    isValidLatitude(latitude) {

        return !isNaN(latitude) && latitude >= -90 && latitude <= 90;
    }

    isValidLongitude(longitude) {

        return !isNaN(longitude) && longitude >= -180 && longitude <= 180;
    }

}