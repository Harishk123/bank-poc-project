import { LightningElement ,track} from 'lwc';
import gethospitalDetail from '@salesforce/apex/hospitalAssociationController.gethospitalDetail';
import getHospitalType from '@salesforce/apex/hospitalAssociationController.getHospitalType';
export default class HospitalManagmentAssociation extends LightningElement {
    
    @track hospitalname;
    @track value;
    @track hospitalExist =true;
    @track latitudeval;
    @track longitudeval;
    @track noHospital;
    @track hospital;
    // @track markerVar;
    @track latVal;
    @track longVal;
    @track myarrayOfmap = [];
    @track optionArray = [];
@track markerVar;
    
    @track hospitalType='Trauma centers';

@track optionArray;
@track uniqueData;
    @track long;
    @track lat;
    @track uniqueValues;
    @track filteredValues;
    @track HospitalDataconvertedTostring;
    @track matchedValue;
  connectedCallback(){
    getHospitalType().then(result => {
        console.log('result>>',result);
        this.uniqueValues = Array.from(new Set(result.map(item => item.Hospital_Type__c)));

        console.log('uniqueValues',this.uniqueValues);

         this.HospitalDataconvertedTostring = this.uniqueValues .filter(Boolean).join(", ");
console.log('this.HospitalDataconvertedTostring>>>',this.HospitalDataconvertedTostring);

    })
    .catch(error => {
        this.error = error;
    });
  }
    @track twoDigits;
    handleKeyUp(event) {
        
        this.queryTerm = event.target.value;
        //  this.filteredValues = this.uniqueValues.filter(value =>
        //     value.toLowerCase().includes(this.queryTerm)
        //   );
        //   console.log('filteredValues',this.filteredValues);
        // this.filteredValues = this.uniqueValues
        // .map(item => item.Hospital_Type__c)
        // .filter(value => value.toLowerCase().includes(this.queryTerm));
  
        if (this.HospitalDataconvertedTostring.toLowerCase().includes(this.queryTerm.toLowerCase())) {
            console.log("Search keyword found in the filtered string.");
          }

//         this.matchedValue = this.uniqueValues.find(item => item && item.toLowerCase().includes(this.queryTerm.toLowerCase()));

// if (matchedValue) {
//   console.log("Matched value:", matchedValue);
// }
     // console.log('this.filteredValues>>>',this.filteredValues);

      //  console.log('this.queryTerm>>>', this.queryTerm);19.077925930402557, 72.87765476561061

        if (this.queryTerm.match(/pune/g)) {
            this.lat = 18.526498235111347;
            console.log('lat>>>>', this.lat);
            this.long = 73.84921616548253;
            
            console.log('long>>>>', this.long);
        }
        // if (this.queryTerm.match(/mumbai/g)) {
        //     this.lat = 19.077925930402557;
        //     console.log('lat>>>>', this.lat);
        //     this.long = 72.87765476561061;
            
        //     console.log('long>>>>', this.long);
        // } 
         else {
            this.lat = 37.790197;
            console.log('lat else>>>>', this.lat);
            this.long = -122.396879;
            
            console.log('long else>>>>', this.long);
        }
    
            let numbers = this.queryTerm.match(/\d+/g);
            console.log('numbers>>>', parseInt(numbers));
           // let twoDigits;
            if (numbers == undefined) {
                this.twoDigits = 10;
                console.log('twoDigits1>>>>>',  this.twoDigits);
            } else {
                this.twoDigits = String(numbers).slice(0, 2);
                console.log('twoDigits2>>>>>',  this.twoDigits);
            }
            
            


        

    }
    handleSubmit(){
console.log('handleSubmit called');
        console.log('this.lat>>><<<<',this.lat);
        console.log('this.long>>><<<<',this.long);
        gethospitalDetail({ distance:this.twoDigits, hospitalType:this.hospitalType, latitudevalue:this.lat, longitudevalue:this.long}).then(result => {
            console.log(' Result>>>', result);
            if (result != null) {
    
                // this.hospitalExist =false;
                for (let i = 0; i < result.length; i++) {
                    this.hospitalname = result[i].Name;
                    this.latitudeval = result[i].Hospital_Location__c.latitude;
                    this.longitudeval = result[i].Hospital_Location__c.longitude;
    
                    let mapobject = {
                        location: {
                            Latitude: this.latitudeval.toString(),
                            Longitude: this.longitudeval.toString(),
    
                        },
                        title: this.hospitalname
    
                    }
                    console.log('mapobject>>', mapobject);
                    this.myarrayOfmap.push(mapobject);
    
                }
                if (this.myarrayOfmap.length > 0) {
                   // this.hospitalExist = false;
                    this.hospital = true;
                    this.noHospital = false;
                }
              
            }else{
                this.hospital = false;
                this.noHospital = true;
            } 
    
        });
    
        console.log('dynamic myarrayofobject>>', this.myarrayOfmap);
    }

}